package com.junior.balinasoft;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.junior.balinasoft.realm.Comment;
import com.junior.balinasoft.realm.User;
import com.junior.balinasoft.retrofit.APP;
import com.junior.balinasoft.retrofit.ApiClasses.DeleteComment.CommentDelete;
import com.junior.balinasoft.retrofit.ApiClasses.GetComments.CommentDownload;
import com.junior.balinasoft.retrofit.ApiClasses.GetComments.Datum;
import com.junior.balinasoft.retrofit.ApiClasses.PostComments.CommentBody;
import com.junior.balinasoft.retrofit.ApiClasses.PostComments.CommentUpload;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PhotoActivity extends AppCompatActivity {

    private ImageView mImageView;
    private ListView mListView;
    private EditText mEditText;
    private Button mButton;

    private long mPhotoId;
    private int mCurrentPage = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mPhotoId = getIntent().getLongExtra("PhotoId", 0);

        mImageView = findViewById(R.id.imageView_photo);
        Picasso.with(this).load(getIntent().getStringExtra("PhotoURL")).placeholder(R.drawable.img).into(mImageView);

        mListView = findViewById(R.id.listView_comments);
        setupAdapter();
        mListView.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int i, int i1, int i2) {
                if(mListView.getChildCount() > 0){
                    if (mListView.getLastVisiblePosition() == mListView.getAdapter().getCount() -1 &&
                            mListView.getChildAt(mListView.getChildCount() - 1).getBottom() <= mListView.getHeight())
                    {
                        getPhoto();
                    }
                }
            }
        });

        mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, final int i, long l) {
                AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(PhotoActivity.this, R.style.AlertDialogCustom));
                builder.setTitle(R.string.delete_comment).setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int j) {

                        final Realm realm = Realm.getDefaultInstance();
                        RealmResults<User> realmUser = realm.where(User.class).equalTo("id", APP.idCurrentUser).findAll();
                        final RealmResults<Comment> realmcomments = realm.where(Comment.class).equalTo("imageId", mPhotoId).findAll().sort("date", Sort.ASCENDING);
                        Map<String, String> map = new HashMap<>();
                        map.put("Access-Token", realmUser.get(0).getToken());
                        Log.d("LESHA", realmcomments.get(i).getId() + "");
                        APP.getApi().deleteComment(mPhotoId, realmcomments.get(i).getId(), map).enqueue(new Callback<CommentDelete>() {
                            @Override
                            public void onResponse(Call<CommentDelete> call, Response<CommentDelete> response) {
                                if(response.code() == 200){
                                    realm.executeTransaction(new Realm.Transaction() {
                                        @Override
                                        public void execute(Realm realm) {
                                            realm.where(Comment.class).equalTo("id", realmcomments.get(i).getId()).findAll().deleteAllFromRealm();
                                        }
                                    });
                                    setupAdapter();
                                } else {
                                    Toast.makeText(getApplicationContext(), getString(R.string.delete_error) + " " + response.code(), Toast.LENGTH_SHORT).show();
                                }
                                realm.close();
                            }

                            @Override
                            public void onFailure(Call<CommentDelete> call, Throwable t) {
                                Toast.makeText(getApplicationContext(), R.string.internet_error, Toast.LENGTH_LONG).show();
                            }
                        });

                    }
                }).setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
                builder.create().show();
                return false;
            }
        });

        mEditText = findViewById(R.id.editText_comment);

        mButton = findViewById(R.id.button_send_comment);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Realm realm = Realm.getDefaultInstance();
                RealmResults<User> realmUser = realm.where(User.class).equalTo("id", APP.idCurrentUser).findAll();
                Map<String, String> map = new HashMap<>();
                map.put("Access-Token", realmUser.get(0).getToken());
                realm.close();

                CommentBody commentBody = new CommentBody();
                commentBody.setText(mEditText.getText().toString());
                mEditText.setText("");

                APP.getApi().postComment(mPhotoId, map, commentBody).enqueue(new Callback<CommentUpload>() {
                    @Override
                    public void onResponse(Call<CommentUpload> call, Response<CommentUpload> response) {

                        if(response.code() != 200){
                            Toast.makeText(PhotoActivity.this, getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                            return;
                        }


                        final Comment comment = new Comment();
                        comment.setDate(response.body().getData().getDate());
                        comment.setId(response.body().getData().getId());
                        comment.setText(response.body().getData().getText());
                        comment.setImageId(mPhotoId);

                        Realm realm = Realm.getDefaultInstance();
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(comment);
                            }
                        });

                        mCurrentPage++;
                        setupAdapter();

                    }

                    @Override
                    public void onFailure(Call<CommentUpload> call, Throwable t) {
                        Toast.makeText(PhotoActivity.this, R.string.internet_error, Toast.LENGTH_LONG).show();
                    }
                });
            }
        });


        getPhoto();

    }

    private void setupAdapter(){

        Realm realm = Realm.getDefaultInstance();
        RealmResults<Comment> realmcomments = realm.where(Comment.class).equalTo("imageId", mPhotoId).findAll().sort("date", Sort.ASCENDING);;
        ArrayList<String> comments = new ArrayList<>();

        comments.clear();

        for (Comment comment : realmcomments){
            comments.add(comment.getText());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, comments);
        mListView.setAdapter(adapter);
    }

    public void getPhoto(){

        Realm realm = Realm.getDefaultInstance();
        RealmResults<User> realmUser = realm.where(User.class).equalTo("id", APP.idCurrentUser).findAll();
        Map<String, String> map = new HashMap<>();
        map.put("Access-Token", realmUser.get(0).getToken());
        realm.close();

        APP.getApi().getComments(mPhotoId, map, mCurrentPage).enqueue(new Callback<CommentDownload>() {
            @Override
            public void onResponse(Call<CommentDownload> call, Response<CommentDownload> response) {
                if(response.code() != 200){
                    Toast.makeText(PhotoActivity.this, getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                    return;
                }

                Realm realm = Realm.getDefaultInstance();

                if(response.body().getData().size() > 0){
                    for(Datum datum : response.body().getData()){

                        final Comment comment = new Comment();
                        comment.setImageId(mPhotoId);
                        comment.setText(datum.getText());
                        comment.setId(datum.getId());
                        comment.setDate(datum.getDate());


                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(comment);
                            }
                        });
                    }


                    mCurrentPage++;
                    setupAdapter();
                } else {
                  //  Toast.makeText(getApplicationContext(), R.string.no_more_comments, Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<CommentDownload> call, Throwable t) {
                Toast.makeText(PhotoActivity.this, R.string.internet_error, Toast.LENGTH_LONG).show();
            }
        });
    }

}
