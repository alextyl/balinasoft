package com.junior.balinasoft;

import android.Manifest;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.junior.balinasoft.Fragments.MapFragment;
import com.junior.balinasoft.Fragments.PhotoFragment;
import com.junior.balinasoft.data.FileHandler;
import com.junior.balinasoft.realm.User;
import com.junior.balinasoft.retrofit.APP;


import java.io.File;

import io.realm.Realm;
import io.realm.RealmResults;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private Toolbar mToolbar;
    private NavigationView mNavigationView;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private FragmentManager mFragmentManager;

    private int mCurrentNavigationId;
    private TextView mTitleName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        if (!FileHandler.makePhotoDir()){
            Toast.makeText(this, getString(R.string.photo_dir_error), Toast.LENGTH_LONG).show();
        }

        mToolbar = findViewById(R.id.app_bar);
        setSupportActionBar(mToolbar);
        mNavigationView = findViewById(R.id.main_navidation);
        mDrawerLayout = findViewById(R.id.drawer_layout);
        mNavigationView.setNavigationItemSelectedListener(this);
        mNavigationView.setCheckedItem(R.id.nav_photo);

        Realm realm = Realm.getDefaultInstance();
        RealmResults<User> realmQuery = realm.where(User.class).equalTo("id",APP.idCurrentUser).findAll();
        mTitleName = mNavigationView.getHeaderView(0).findViewById(R.id.title_name);
        mTitleName.setText(realmQuery.get(0).getName());
        realm.close();

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                mToolbar, R.string.drawer_open, R.string.drawer_close);
        mDrawerLayout.addDrawerListener(mDrawerToggle);

        mDrawerToggle.syncState();


        mFragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = mFragmentManager
                .beginTransaction();
        fragmentTransaction.add(R.id.fragment, new PhotoFragment());
        fragmentTransaction.commit();


        int permission = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    0
            );}

    }


    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        mDrawerLayout.closeDrawer(GravityCompat.START);

        if(mCurrentNavigationId == item.getItemId()) return false;
        mCurrentNavigationId = item.getItemId();

        if(item.getItemId() == R.id.nav_photo){
            FragmentTransaction fragmentTransaction = mFragmentManager
                    .beginTransaction();
            fragmentTransaction.replace(R.id.fragment, new PhotoFragment());
            fragmentTransaction.commit();
        } else {
            FragmentTransaction fragmentTransaction = mFragmentManager
                    .beginTransaction();
            fragmentTransaction.replace(R.id.fragment, new MapFragment());
            fragmentTransaction.commit();
        }


        return true;
    }
}
