package com.junior.balinasoft.retrofit.ApiClasses.PostPhoto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PhotoUpload {

    @SerializedName("status")
    @Expose
    private long status;
    @SerializedName("data")
    @Expose
    private Data data;

    public long getStatus() {
        return status;
    }

    public void setStatus(long status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

}