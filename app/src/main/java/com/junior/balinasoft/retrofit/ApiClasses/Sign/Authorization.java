package com.junior.balinasoft.retrofit.ApiClasses.Sign;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Authorization {

    @SerializedName("status")
    @Expose
    private long status;

    @SerializedName("data")
    @Expose
    private Data data;

    @SerializedName("error")
    @Expose
    private String error;

    @SerializedName("valid")
    @Expose
    private List<Valid> valid = null;

    public long getStatus() {
        return status;
    }

    public void setStatus(long status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public List<Valid> getValid() {
        return valid;
    }

    public void setValid(List<Valid> valid) {
        this.valid = valid;
    }
}
