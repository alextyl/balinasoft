package com.junior.balinasoft.retrofit.ApiClasses.PostComments;

/**
 * Created by Алексей on 21.02.2018.
 */

public class CommentBody {

    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
