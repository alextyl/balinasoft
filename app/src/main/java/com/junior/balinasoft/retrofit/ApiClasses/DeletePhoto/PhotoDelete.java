package com.junior.balinasoft.retrofit.ApiClasses.DeletePhoto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class PhotoDelete {

    @SerializedName("status")
    @Expose
    private long status;


    public long getStatus() {
        return status;
    }

    public void setStatus(long status) {
        this.status = status;
    }



}
