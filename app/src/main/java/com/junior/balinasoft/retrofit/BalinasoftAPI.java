package com.junior.balinasoft.retrofit;


import com.junior.balinasoft.retrofit.ApiClasses.DeleteComment.CommentDelete;
import com.junior.balinasoft.retrofit.ApiClasses.DeletePhoto.PhotoDelete;
import com.junior.balinasoft.retrofit.ApiClasses.GetComments.CommentDownload;
import com.junior.balinasoft.retrofit.ApiClasses.GetPhoto.PhotoDownload;
import com.junior.balinasoft.retrofit.ApiClasses.PostComments.CommentBody;
import com.junior.balinasoft.retrofit.ApiClasses.PostComments.CommentUpload;
import com.junior.balinasoft.retrofit.ApiClasses.PostPhoto.PhotoPostBody;
import com.junior.balinasoft.retrofit.ApiClasses.PostPhoto.PhotoUpload;
import com.junior.balinasoft.retrofit.ApiClasses.Sign.Authorization;
import com.junior.balinasoft.retrofit.ApiClasses.Sign.AuthorizationBody;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface BalinasoftAPI {

    @POST("/api/account/signup")
    Call<Authorization> getRegistration(@Body AuthorizationBody authorizationBody);

    @POST("/api/account/signin")
    Call<Authorization> getAuthorization(@Body AuthorizationBody authorizationBody);

    @POST("/api/image")
    Call<PhotoUpload> postPhoto(@Body PhotoPostBody photoPostBody, @HeaderMap Map<String, String> headers);

    @GET("/api/image")
    Call<PhotoDownload> getPhoto(@Query("page") int page, @HeaderMap Map<String, String> headers);

    @DELETE("/api/image/{id}")
    Call<PhotoDelete> deletePhoto(@Path("id") long id, @HeaderMap Map<String, String> headers);

    @POST("/api/image/{imageId}/comment")
    Call<CommentUpload> postComment(@Path("imageId") long id, @HeaderMap Map<String, String> headers, @Body CommentBody commentBody);

    @GET("/api/image/{imageId}/comment")
    Call<CommentDownload> getComments(@Path("imageId") long id, @HeaderMap Map<String, String> headers, @Query("page") int page);

    @DELETE("/api/image/{imageId}/comment/{commentId}")
    Call<CommentDelete> deleteComment(@Path("imageId") long imageId, @Path("commentId") long commentId, @HeaderMap Map<String, String> headers);

}
