package com.junior.balinasoft.retrofit;


import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APP extends Application {

    public static volatile long idCurrentUser;

    private static BalinasoftAPI balinadoftApi;
    private Retrofit retrofit;


    @Override
    public void onCreate() {
        super.onCreate();

        retrofit = new Retrofit.Builder()
                .baseUrl("http://junior.balinasoft.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        balinadoftApi = retrofit.create(BalinasoftAPI.class);

        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder().name("balinasoft.realm")
                .schemaVersion(0)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(config);

    }

    public static BalinasoftAPI getApi() {
        return balinadoftApi;
    }

}
