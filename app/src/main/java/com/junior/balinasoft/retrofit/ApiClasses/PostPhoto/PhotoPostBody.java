package com.junior.balinasoft.retrofit.ApiClasses.PostPhoto;


public class PhotoPostBody {


    private double lat;
    private double lng;
    private long date;
    private String base64Image;

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getBase64Image() {
        return base64Image;
    }

    public void setBase64Image(String base64Image) {
        this.base64Image = base64Image;
    }

    @Override
    public String toString() {
        return base64Image + " " +
                String.valueOf(date) + " " +
                String.valueOf(lat) + " " +
                String.valueOf(lng);
    }
}
