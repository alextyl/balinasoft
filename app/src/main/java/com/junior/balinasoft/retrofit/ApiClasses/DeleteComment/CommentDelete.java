package com.junior.balinasoft.retrofit.ApiClasses.DeleteComment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CommentDelete {

    @SerializedName("date")
    @Expose
    private long date;
    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("text")
    @Expose
    private String text;

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}