package com.junior.balinasoft.retrofit.ApiClasses.GetPhoto;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PhotoDownload {

    @SerializedName("status")
    @Expose
    private long status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public long getStatus() {
        return status;
    }

    public void setStatus(long status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

}