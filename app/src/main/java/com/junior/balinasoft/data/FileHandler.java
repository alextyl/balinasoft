package com.junior.balinasoft.data;



import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Environment;
import android.util.Base64;


import java.io.ByteArrayOutputStream;
import java.io.File;


public class FileHandler {

    private static File mDirectory;

    public static boolean makePhotoDir() {

        mDirectory = new File(Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "BalinaSoft");
        return mDirectory.exists() || mDirectory.mkdirs();
    }

    public static File getDirectory() {
        return mDirectory;
    }

    public static String convertToBase64(String path)  {
        Bitmap bitmapSource = BitmapFactory.decodeFile(path);
        int w = bitmapSource.getWidth();
        int h = bitmapSource.getHeight();
        int heigh = 0;
        int width = 0;

        if(h > 1280 || w > 1280){
            if(h >w ){
                heigh = 1280;
                width = 1280 * w / h;
            }else if(w >= h){
                width = 1280;
                heigh = 1280 * h /w;
            }
        } else {
            heigh = h ;
            width = w;
        }

        Matrix matrix = new Matrix();
        if(h<w)matrix.postRotate(90);
        if(h>w)matrix.postRotate(0);

        Bitmap bitmapsource2=Bitmap.createScaledBitmap(bitmapSource, width, heigh, false);

        Bitmap bitmap=Bitmap.createBitmap(bitmapsource2, 0, 0, bitmapsource2.getWidth(), bitmapsource2.getHeight(), matrix, true);

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        int quality = 100;


        while (true){
            try {
                if(quality < 0){
                    return null;
                }
                bitmap.compress(Bitmap.CompressFormat.JPEG, quality, byteArrayOutputStream);
                return Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
            } catch (OutOfMemoryError e){
                e.fillInStackTrace();
                byteArrayOutputStream = new ByteArrayOutputStream();
                quality -= 10;
            }
        }


    }
}


