package com.junior.balinasoft;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.junior.balinasoft.Fragments.AuthorizationFragment;
import com.junior.balinasoft.Fragments.RegistrationFragment;

public class LoginActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private PagerAdapter mPagerAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mPagerAdapter = new PagerAdapter(getSupportFragmentManager());
        mToolbar = findViewById(R.id.log_reg_bar);
        setSupportActionBar(mToolbar);
        mTabLayout = findViewById(R.id.log_reg_tab);
        mViewPager = findViewById(R.id.log_reg_pager);
        mViewPager.setAdapter(mPagerAdapter);
        mTabLayout.setupWithViewPager(mViewPager);
    }

    public class PagerAdapter extends FragmentStatePagerAdapter {

        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return position == 0 ? new AuthorizationFragment() : new RegistrationFragment();
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return position == 0 ? getString(R.string.login_text) : getString(R.string.registration_text);
        }
    }
}
