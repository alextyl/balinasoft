package com.junior.balinasoft.Fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.junior.balinasoft.PhotoActivity;
import com.junior.balinasoft.R;
import com.junior.balinasoft.data.FileHandler;
import com.junior.balinasoft.realm.Photo;
import com.junior.balinasoft.realm.User;
import com.junior.balinasoft.retrofit.APP;
import com.junior.balinasoft.retrofit.ApiClasses.DeletePhoto.PhotoDelete;
import com.junior.balinasoft.retrofit.ApiClasses.GetPhoto.Datum;
import com.junior.balinasoft.retrofit.ApiClasses.GetPhoto.PhotoDownload;
import com.junior.balinasoft.retrofit.ApiClasses.PostPhoto.PhotoPostBody;
import com.junior.balinasoft.retrofit.ApiClasses.PostPhoto.PhotoUpload;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static android.support.v4.content.ContextCompat.checkSelfPermission;


public class PhotoFragment extends Fragment implements LocationListener {

    private static final int MAKE_PHOTO = 1;
    private static final int LOCATION_REQUEST_CODE = 2;
    private static final int CAMERA_REQUEST_CODE = 100;

    private RecyclerView mRecyclerView;
    private GridLayoutManager mGridLayoutManager;
    private List<Photo> mPhotos = new ArrayList<>();

    private FloatingActionButton mFloatingActionButton;

    private int mCurrentPage = 0;
    private Location mLocation;
    private String mLastPhotoPath;
    private LocationManager mLocationManager;
    private boolean mLazyLoading = true;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_photo, container, false);

        mRecyclerView = view.findViewById(R.id.recycker);
        mGridLayoutManager = new GridLayoutManager(getActivity(), 3);
        mRecyclerView.setLayoutManager(mGridLayoutManager);
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if ((mGridLayoutManager.findLastCompletelyVisibleItemPosition() + 1) == mGridLayoutManager.getItemCount()) {
                        if(!mLazyLoading){
                            mLazyLoading = true;

                            final Realm realm = Realm.getDefaultInstance();
                            RealmResults<User> realmQuery = realm.where(User.class).equalTo("id", APP.idCurrentUser).findAll();
                            Map<String, String> map = new HashMap<>();
                            map.put("Access-Token", realmQuery.get(0).getToken());
                            APP.getApi().getPhoto(mCurrentPage, map).enqueue(new Callback<PhotoDownload>() {
                                @Override
                                public void onResponse(Call<PhotoDownload> call, Response<PhotoDownload> response) {
                                    if(response.code() != 200){
                                        Toast.makeText(getActivity(), getString(R.string.get_photo_error), Toast.LENGTH_SHORT).show();
                                        realm.close();
                                        mLazyLoading = false;
                                        return;
                                    }

                                    if(response.body().getData().size() > 0){
                                        for(Datum datum : response.body().getData()){
                                            final Photo photo = new Photo();
                                            photo.setUrl(datum.getUrl());
                                            photo.setId(datum.getId());
                                            photo.setDate(datum.getDate());
                                            photo.setLat(datum.getLat());
                                            photo.setLng(datum.getLng());
                                            photo.setIdUser(APP.idCurrentUser);

                                            realm.executeTransaction(new Realm.Transaction() {
                                                @Override
                                                public void execute(Realm realm) {
                                                    realm.copyToRealmOrUpdate(photo);
                                                }
                                            });
                                        }

                                        Toast.makeText(getActivity(), R.string.photo_get, Toast.LENGTH_SHORT).show();

                                        realm.close();

                                        mCurrentPage++;

                                        setupAdapter();

                                    } else {
                                        Toast.makeText(getActivity(), R.string.no_more_photo, Toast.LENGTH_SHORT).show();
                                    }

                                    mLazyLoading = false;

                                }

                                @Override
                                public void onFailure(Call<PhotoDownload> call, Throwable t) {
                                    mLazyLoading = false;
                                    Toast.makeText(getContext(), R.string.internet_error, Toast.LENGTH_LONG).show();
                                    realm.close();
                                }
                            });
                        }
                }
            }
        });

        final Realm realm = Realm.getDefaultInstance();
        final RealmResults<User> realmUser = realm.where(User.class).equalTo("id", 584).findAll();
        Map<String, String> map = new HashMap<>();
        map.put("Access-Token", realmUser.get(0).getToken());

        APP.getApi().getPhoto(mCurrentPage, map).enqueue(new Callback<PhotoDownload>() {
            @Override
            public void onResponse(Call<PhotoDownload> call, Response<PhotoDownload> response) {
                if(response.code() != 200){
                    Toast.makeText(getActivity(), getString(R.string.get_photo_error), Toast.LENGTH_SHORT).show();
                    realm.close();
                    mLazyLoading = false;
                    return;
                }

                for(Datum datum : response.body().getData()){
                    final Photo photo = new Photo();
                    photo.setUrl(datum.getUrl());
                    photo.setId(datum.getId());
                    photo.setDate(datum.getDate());
                    photo.setLat(datum.getLat());
                    photo.setLng(datum.getLng());
                    photo.setIdUser(APP.idCurrentUser);

                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            realm.copyToRealmOrUpdate(photo);
                        }
                    });
                }

                Toast.makeText(getActivity(), R.string.photo_get, Toast.LENGTH_SHORT).show();

                realm.close();

                setupAdapter();

                mCurrentPage++;

                mLazyLoading = false;

            }

            @Override
            public void onFailure(Call<PhotoDownload> call, Throwable t) {
                Toast.makeText(getContext(), R.string.internet_error, Toast.LENGTH_LONG).show();
                realm.close();
                mLazyLoading = false;
            }
        });


        mFloatingActionButton = view.findViewById(R.id.fab_photo);
        mFloatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkSelfPermission(PhotoFragment.this.getActivity() , Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA},
                            CAMERA_REQUEST_CODE);
                } else {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    Uri uri = Uri.fromFile(new File(FileHandler.getDirectory().getPath()
                            + "/" + "photo_"
                            + System.currentTimeMillis() + ".jpg"));
                    mLastPhotoPath = uri.getPath();
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                    startActivityForResult(intent, MAKE_PHOTO);
                }
            }
        });


        if (checkSelfPermission(PhotoFragment.this.getActivity() , Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    LOCATION_REQUEST_CODE);
        } else {
            mLocationManager = (LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);
            try {
                mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
            } catch (NullPointerException e){
                e.printStackTrace();
            }
        }

        return view;
    }


    private void setupAdapter(){
        if(isAdded()){
            Realm realm = Realm.getDefaultInstance();
            RealmResults<Photo> realmPhotos = realm.where(Photo.class).equalTo("idUser", APP.idCurrentUser).findAll().sort("date", Sort.ASCENDING);
            mPhotos.clear();

            for(int i = 0; i < (mCurrentPage + 1) * 20; i++){
                if(i == realmPhotos.size()){
                    break;
                }
                mPhotos.add(realmPhotos.get(i));
            }


            mRecyclerView.setAdapter(new PhotoAdapter(mPhotos));
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == MAKE_PHOTO){
            if(resultCode == RESULT_OK && (checkSelfPermission(PhotoFragment.this.getActivity() , Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED)){
                if(mLocation == null){
                    mLocation = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                }

                final long timestump = System.currentTimeMillis() / 1000;
                final String base64 = FileHandler.convertToBase64(mLastPhotoPath);

                final Realm realm = Realm.getDefaultInstance();

                final PhotoPostBody photoPostBody = new PhotoPostBody();
                photoPostBody.setBase64Image(base64);
                photoPostBody.setDate(timestump);
                photoPostBody.setLat(mLocation.getLatitude());
                photoPostBody.setLng(mLocation.getLongitude());

                RealmResults<User> realmUser = realm.where(User.class).equalTo("id", APP.idCurrentUser).findAll();

                Map<String, String> map = new HashMap<>();
                map.put("Access-Token", realmUser.get(0).getToken());

                APP.getApi().postPhoto(photoPostBody, map).enqueue(new Callback<PhotoUpload>() {
                    @Override
                    public void onResponse(Call<PhotoUpload> call, final Response<PhotoUpload> response) {


                        if (response.code() != 200){
                            Toast.makeText(getContext(), getString(R.string.upload_photo_error) + response.code(), Toast.LENGTH_LONG).show();
                            realm.close();
                        }


                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(@NonNull Realm realm) {
                                Photo photo = new Photo();
                                photo.setLat(mLocation.getLatitude());
                                photo.setLng(mLocation.getLongitude());
                                photo.setIdUser(APP.idCurrentUser);
                                photo.setDate(timestump);
                                photo.setBase64Image(base64);
                                photo.setId(response.body().getData().getId());
                                photo.setUrl(response.body().getData().getUrl());
                                realm.copyToRealmOrUpdate(photo);
                            }
                        });

                        Toast.makeText(getContext(), getString(R.string.photo_uploaded), Toast.LENGTH_LONG).show();

                        realm.close();

                        setupAdapter();
                    }

                    @Override
                    public void onFailure(Call<PhotoUpload> call, Throwable t) {
                        Toast.makeText(getContext(), R.string.internet_error, Toast.LENGTH_LONG).show();
                        realm.close();
                    }
                });

            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if (checkSelfPermission(PhotoFragment.this.getActivity() , Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED){
            mLocation = location;
        }
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mLocationManager.removeUpdates(this);
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == CAMERA_REQUEST_CODE) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, MAKE_PHOTO);

            } else {
                Toast.makeText(this.getActivity(), R.string.cam_per_den, Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == LOCATION_REQUEST_CODE){

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                mLocationManager = (LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);
                try {
                    mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
                } catch (NullPointerException e){
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(this.getActivity(), "Need permission to Network!", Toast.LENGTH_LONG).show();
            }

        }
    }

    private class PhotoHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener, View.OnClickListener {

        private AppCompatImageView mImageView;
        private TextView mTextView;

        private long id;
        private String url;

        public PhotoHolder(View itemView) {
            super(itemView);

            itemView.setOnLongClickListener(this);
            itemView.setOnClickListener(this);
            mTextView = itemView.findViewById(R.id.text_data);
            mImageView = itemView.findViewById(R.id.item_image_view);
        }

        public void bindGalleryItem(Photo photo){
            id = photo.getId();
            url = photo.getUrl();
            mTextView.setText(new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault()).format(new Date(photo.getDate() * 1000)));
            Picasso.with(getActivity()).load(photo.getUrl()).placeholder(R.drawable.img).fit().into(mImageView);
        }

        @Override
        public boolean onLongClick(View view) {
            AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.AlertDialogCustom));
            builder.setTitle(R.string.is_delete_photo).setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                    final Realm realm = Realm.getDefaultInstance();
                    RealmResults<User> realmUser = realm.where(User.class).equalTo("id", APP.idCurrentUser).findAll();
                    Map<String, String> map = new HashMap<>();
                    map.put("Access-Token", realmUser.get(0).getToken());
                    APP.getApi().deletePhoto(id, map).enqueue(new Callback<PhotoDelete>() {
                        @Override
                        public void onResponse(Call<PhotoDelete> call, Response<PhotoDelete> response) {
                            if(response.code() == 200){
                                realm.executeTransaction(new Realm.Transaction() {
                                    @Override
                                    public void execute(Realm realm) {
                                        realm.where(Photo.class).equalTo("id", id).findAll().deleteAllFromRealm();
                                    }
                                });
                                setupAdapter();
                            } else {
                                Toast.makeText(getActivity(), R.string.delete_error, Toast.LENGTH_SHORT).show();
                            }
                            realm.close();
                        }

                        @Override
                        public void onFailure(Call<PhotoDelete> call, Throwable t) {
                            Toast.makeText(getContext(), R.string.internet_error, Toast.LENGTH_LONG).show();
                        }
                    });

                }
            }).setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });
            builder.create().show();
            return false;
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(PhotoFragment.this.getActivity(), PhotoActivity.class);
            intent.putExtra("PhotoURL", url);
            intent.putExtra("PhotoId", id);
            PhotoFragment.this.startActivity(intent);
        }
    }

    private class PhotoAdapter extends RecyclerView.Adapter<PhotoHolder>{

        private List<Photo> mPhotos;

        public PhotoAdapter(List<Photo> photos){
            this.mPhotos = photos;
        }

        @Override
        public PhotoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(getActivity());
            View view = inflater.inflate(R.layout.gallery_item, parent, false);
            return new PhotoHolder(view);
        }

        @Override
        public void onBindViewHolder(PhotoHolder holder, int position) {
            Photo photo = mPhotos.get(position);
            holder.bindGalleryItem(photo);
        }

        @Override
        public int getItemCount() {
            return mPhotos.size();
        }
    }

}
