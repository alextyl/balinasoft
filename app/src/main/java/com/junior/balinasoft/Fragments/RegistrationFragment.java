package com.junior.balinasoft.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.junior.balinasoft.R;
import com.junior.balinasoft.retrofit.ApiClasses.Sign.Authorization;
import com.junior.balinasoft.retrofit.ApiClasses.Sign.AuthorizationBody;
import com.junior.balinasoft.retrofit.APP;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class RegistrationFragment extends Fragment {

    private EditText mLogin;
    private EditText mPassword;
    private EditText mRepeatPassword;
    private Button mSingUp;
    private ProgressBar mProgressBar;

    public RegistrationFragment(){

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_registration, container, false);

        mLogin = view.findViewById(R.id.reg_text);
        mPassword = view.findViewById(R.id.reg_pass_text);
        mRepeatPassword = view.findViewById(R.id.reg_pass_rep_text);
        mSingUp = view.findViewById(R.id.reg_button);
        mProgressBar = view.findViewById(R.id.progressBar);
        mSingUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!mPassword.getText().toString().equals(mRepeatPassword.getText().toString())){
                    Toast.makeText(getContext(), R.string.check_pass, Toast.LENGTH_LONG).show();
                    return;
                }

                AuthorizationBody authorizationBody = new AuthorizationBody();
                authorizationBody.setLogin(mLogin.getText().toString());
                authorizationBody.setPassword(mPassword.getText().toString());

                mProgressBar.setVisibility(View.VISIBLE);

                APP.getApi().getRegistration(authorizationBody).enqueue(new Callback<Authorization>() {
                    @Override
                    public void onResponse(Call<Authorization> call, Response<Authorization> response) {
                        if (response.code() != 200){
                            Toast.makeText(getContext(), getString(R.string.reg_error) + response.code(), Toast.LENGTH_LONG).show();
                            mProgressBar.setVisibility(View.INVISIBLE);
                            return;
                        }
                        Toast.makeText(getContext(), R.string.reg_ok, Toast.LENGTH_LONG).show();
                        mLogin.setText("");
                        mPassword.setText("");
                        mRepeatPassword.setText("");
                        mProgressBar.setVisibility(View.INVISIBLE);

                    }

                    @Override
                    public void onFailure(Call<Authorization> call, Throwable t) {
                        Toast.makeText(getContext(), R.string.internet_error, Toast.LENGTH_LONG).show();
                        mProgressBar.setVisibility(View.INVISIBLE);
                    }
                });
            }
        });

        return view;
    }
}
