package com.junior.balinasoft.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.junior.balinasoft.MainActivity;
import com.junior.balinasoft.R;
import com.junior.balinasoft.realm.User;
import com.junior.balinasoft.retrofit.ApiClasses.Sign.Authorization;
import com.junior.balinasoft.retrofit.ApiClasses.Sign.AuthorizationBody;
import com.junior.balinasoft.retrofit.APP;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AuthorizationFragment extends Fragment {

    private EditText mLogin;
    private EditText mPassword;
    private Button mSingIn;
    private ProgressBar mProgressBar;

    public AuthorizationFragment(){

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_authorization, container, false);

        mProgressBar = view.findViewById(R.id.progressBar2);
        mLogin = view.findViewById(R.id.log_text);
        mPassword = view.findViewById(R.id.pass_text);
        mSingIn = view.findViewById(R.id.singin_button);
        mSingIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                AuthorizationBody authorizationBody = new AuthorizationBody();
                authorizationBody.setLogin(mLogin.getText().toString());
                authorizationBody.setPassword(mPassword.getText().toString());

                mProgressBar.setVisibility(View.VISIBLE);

                APP.getApi().getAuthorization(authorizationBody).enqueue(new Callback<Authorization>() {
                    @Override
                    public void onResponse(Call<Authorization> call, Response<Authorization> response) {

                        try {

                            mLogin.setText("");
                            mPassword.setText("");

                            APP.idCurrentUser = response.body().getData().getUserId();

                            Realm realm = Realm.getDefaultInstance();

                            final User user = new User();
                            user.setId(response.body().getData().getUserId());
                            user.setName(response.body().getData().getLogin());
                            user.setToken(response.body().getData().getToken());

                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.copyToRealmOrUpdate(user);
                                }
                            });
                            realm.close();

                            Toast.makeText(getContext(), R.string.log_ok, Toast.LENGTH_LONG).show();

                            Intent intent = new Intent(getContext(), MainActivity.class);
                            startActivity(intent);
                        } catch (NullPointerException e){
                            e.printStackTrace();
                            Toast.makeText(getContext(), getString(R.string.log_error) + response.code(), Toast.LENGTH_LONG).show();
                        } finally {
                            mProgressBar.setVisibility(View.INVISIBLE);
                        }

                    }

                    @Override
                    public void onFailure(Call<Authorization> call, Throwable t) {
                        Toast.makeText(getContext(), R.string.internet_error, Toast.LENGTH_LONG).show();
                        mProgressBar.setVisibility(View.INVISIBLE);
                    }
                });
            }
        });

        return view;
    }
}
