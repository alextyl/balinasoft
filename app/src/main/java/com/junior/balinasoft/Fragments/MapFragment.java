package com.junior.balinasoft.Fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.junior.balinasoft.R;
import com.junior.balinasoft.data.FileHandler;
import com.junior.balinasoft.realm.Photo;
import com.junior.balinasoft.realm.User;
import com.junior.balinasoft.retrofit.APP;
import com.junior.balinasoft.retrofit.ApiClasses.PostPhoto.PhotoPostBody;
import com.junior.balinasoft.retrofit.ApiClasses.PostPhoto.PhotoUpload;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static android.support.v4.content.ContextCompat.checkSelfPermission;


public class MapFragment extends Fragment implements OnMapReadyCallback, LocationListener {


    private static final int MAKE_PHOTO = 1;
    private static final int LOCATION_REQUEST_CODE = 2;
    private static final int CAMERA_REQUEST_CODE = 100;

    private GoogleMap mMap;
    private MapView mMapView;
    private FloatingActionButton mFloatingActionButton;
    private LocationManager mLocationManager;

    private Location mLocation;
    private String mLastPhotoPath;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_map, container, false);

        mMapView = view.findViewById(R.id.mapView);

        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(this);

        mFloatingActionButton = view.findViewById(R.id.fab_map);
        mFloatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkSelfPermission(MapFragment.this.getActivity() , Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA},
                            CAMERA_REQUEST_CODE);
                } else {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    Uri uri = Uri.fromFile(new File(FileHandler.getDirectory().getPath()
                            + "/" + "photo_"
                            + System.currentTimeMillis() + ".jpg"));
                    mLastPhotoPath = uri.getPath();
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                    startActivityForResult(intent, MAKE_PHOTO);
                }
            }
        });


        if (checkSelfPermission(MapFragment.this.getActivity() , Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    LOCATION_REQUEST_CODE);
        } else {
            mLocationManager = (LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);
            try {
                mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
            } catch (NullPointerException e){
                e.printStackTrace();
            }
        }


        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        LatLng minsk = new LatLng(53.776952, 27.389405);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(minsk, 9));

        Realm realm = Realm.getDefaultInstance();
        RealmResults<Photo> realmQuery = realm.where(Photo.class).
                equalTo("idUser", APP.idCurrentUser).findAll();
        for (Photo photo : realmQuery){
            LatLng latLng = new LatLng(photo.getLat(), photo.getLng());
            mMap.addMarker(new MarkerOptions().position(latLng).title(latLng.toString()));
        }
        realm.close();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == MAKE_PHOTO){
            if(resultCode == RESULT_OK && (checkSelfPermission(MapFragment.this.getActivity() , Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED)){
                if(mLocation == null){
                   mLocation = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                }

                final long timestump = System.currentTimeMillis() / 1000;
                final String base64 = FileHandler.convertToBase64(mLastPhotoPath);

                final Realm realm = Realm.getDefaultInstance();

                final PhotoPostBody photoPostBody = new PhotoPostBody();
                photoPostBody.setBase64Image(base64);
                photoPostBody.setDate(timestump);
                photoPostBody.setLat(mLocation.getLatitude());
                photoPostBody.setLng(mLocation.getLongitude());

                RealmResults<User> realmUser = realm.where(User.class).equalTo("id", APP.idCurrentUser).findAll();

                Map<String, String> map = new HashMap<>();
                map.put("Access-Token", realmUser.get(0).getToken());

                APP.getApi().postPhoto(photoPostBody, map).enqueue(new Callback<PhotoUpload>() {
                    @Override
                    public void onResponse(Call<PhotoUpload> call, final Response<PhotoUpload> response) {


                        if (response.code() != 200){
                            Toast.makeText(getContext(), getString(R.string.upload_photo_error) + response.code(), Toast.LENGTH_LONG).show();
                            realm.close();
                        }


                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(@NonNull Realm realm) {
                                Photo photo = new Photo();
                                photo.setLat(mLocation.getLatitude());
                                photo.setLng(mLocation.getLongitude());
                                photo.setIdUser(APP.idCurrentUser);
                                photo.setDate(timestump);
                                photo.setBase64Image(base64);
                                photo.setId(response.body().getData().getId());
                                photo.setUrl(response.body().getData().getUrl());
                                realm.copyToRealmOrUpdate(photo);
                            }
                        });


                        RealmResults<Photo> realmQuery = realm.where(Photo.class).
                                equalTo("idUser", APP.idCurrentUser).findAll();
                        for (Photo photo : realmQuery){
                            LatLng latLng = new LatLng(photo.getLat(), photo.getLng());
                            mMap.addMarker(new MarkerOptions().position(latLng).title(latLng.toString()));
                        }

                        Toast.makeText(getContext(), getString(R.string.photo_uploaded), Toast.LENGTH_LONG).show();

                        realm.close();
                    }

                    @Override
                    public void onFailure(Call<PhotoUpload> call, Throwable t) {
                        Toast.makeText(getContext(), R.string.internet_error, Toast.LENGTH_LONG).show();
                        realm.close();
                    }
                });

            }
        }
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == CAMERA_REQUEST_CODE) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, MAKE_PHOTO);

            } else {
                Toast.makeText(this.getActivity(), R.string.cam_per_den, Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == LOCATION_REQUEST_CODE){

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                mLocationManager = (LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);
                try {
                    mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
                } catch (NullPointerException e){
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(this.getActivity(), "Need permission to Network!", Toast.LENGTH_LONG).show();
            }

        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mLocationManager.removeUpdates(this);
    }


    @Override
    public void onLocationChanged(Location location) {
        if (checkSelfPermission(MapFragment.this.getActivity() , Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED){
            mLocation = location;
        }
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}
