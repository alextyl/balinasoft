package com.junior.balinasoft.realm;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;



public class Comment extends RealmObject {

    @PrimaryKey
    private long id;

    private long imageId;

    private long date;
    private String text;

    @Override
    public String toString() {
        return this.getText();
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public long getImageId() {
        return imageId;
    }

    public void setImageId(long imageId) {
        this.imageId = imageId;
    }
}
